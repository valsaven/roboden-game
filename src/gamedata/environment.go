package gamedata

type EnvironmentKind int

const (
	EnvMoon EnvironmentKind = iota
	EnvForest
)
